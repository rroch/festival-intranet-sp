<?php

namespace controleur;

use controleur\GestionErreurs;
use modele\dao\GroupeDAO;
use modele\dao\AttributionDAO;
use modele\metier\Groupe;
use modele\dao\Bdd;
use vue\groupes\VueListeGroupes;
use vue\groupes\VueDetailGroupe;
use vue\groupes\VueSaisieGroupe;
use vue\groupes\VueSupprimerGroupe;


class CtrlGroupes extends ControleurGenerique {
    /** controleur= Groupes & action= defaut
     * Afficher la liste des établissements      */
  public function defaut() {
        $this->liste();
    }

    /** controleur= etablissements & action= liste
     * Afficher la liste des établissements      */
    public function liste() {
        $laVue = new VueListeGroupes();
        $this->vue = $laVue;
        // On récupère un tableau composé de la liste des établissements avec, pour chacun,
        //  son nombre d'attributions de chambres actuel : 
        //  on ne peut supprimer un établissement que si aucune chambre ne lui est actuellement attribuée
        Bdd::connecter();
        $laVue->setLesGroupesAvecNbAttributions($this->getTabGroupesAvecNbAttributions());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - etablissements");
        $this->vue->afficher();
    }

    /** controleur= Groupes & action=detail & id=identifiant_établissement
     * Afficher un établissement d'après son identifiant     */
    public function detail() {
        $idGroupe = $_GET["id"];
        $this->vue = new VueDetailGroupe();
        // Lire dans la BDD les données de l'établissement à afficher
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGroupe));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - etablissements");
        $this->vue->afficher();
    }

    public function creer() {
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        $laVue->setActionRecue("creer");
        $laVue->setActionAEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau Groupe");
        // En création, on affiche un formulaire vide
        /* @var Etablissement $unEtab */
        $unGrp = new Groupe("", "", "", "", "", "", "O");
        $laVue->setUnGroupe($unGrp);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }
    
    public function validerCreer() {
        Bdd::connecter();
        /* @var Etablissement $unEtab  : récupération du contenu du formulaire et instanciation d'un établissement */
        $unGrp = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identiteResponsable'], $_REQUEST['adressePostale'], $_REQUEST['nombrePersonnes'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);
        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de création (paramètre n°1 = true)
        $this->verifierDonneesEtab($unGrp, true);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer l'établissement
            GroupeDAO::insert($unGrp);
            // revenir à la liste des établissements
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de création
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setActionRecue("creer");
            $laVue->setActionAEnvoyer("validerCreer");
            $laVue->setMessage("Nouveau Groupe");
            $laVue->setUnGroupe($unGrp);
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - Groupes");
            $this->vue->afficher();
        }
    }
    /** controleur= etablissements & action=modifier $ id=identifiant de l'établissement à modifier
     * Afficher le formulaire de modification d'un établissement     */
    public function modifier() {
        $idGrp = $_GET["id"];
        $laVue = new VueSaisieGroupe();
        $this->vue = $laVue;
        // Lire dans la BDD les données de l'établissement à modifier
        Bdd::connecter();
        /* @var Etablissement $leEtablissement */
        $leGroupe = GroupeDAO::getOneById($idGrp);
        $this->vue->setUnGroupe($leGroupe);
        $laVue->setActionRecue("modifier");
        $laVue->setActionAEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le Groupe : " . $leGroupe->getNom() . " (" . $leGroupe->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    /** controleur= Groupes & action=validerModifier
     * modifier un établissement dans la base de données d'après la saisie    */
    public function validerModifier() {
        Bdd::connecter();
        /* @var Groupe $unEtab  : récupération du contenu du formulaire et instanciation d'un établissement */
        $unGrp = new Groupe($_REQUEST['id'], $_REQUEST['nom'], $_REQUEST['identiteResponsable'], $_REQUEST['adressePostale'], $_REQUEST['nombrePersonnes'], $_REQUEST['nomPays'], $_REQUEST['hebergement']);

        // vérifier la saisie des champs obligatoires et les contraintes d'intégrité du contenu
        // pour un formulaire de modification (paramètre n°1 = false)
        $this->verifierDonneesEtab($unGrp, false);
        if (GestionErreurs::nbErreurs() == 0) {
            // s'il ny a pas d'erreurs,
            // enregistrer les modifications pour l'établissement
            GroupeDAO::update($unGrp->getId(), $unGrp);
            // revenir à la liste des établissements
            header("Location: index.php?controleur=groupes&action=liste");
        } else {
            // s'il y a des erreurs, 
            // réafficher le formulaire de modification
            $laVue = new VueSaisieGroupe();
            $this->vue = $laVue;
            $laVue->setUnGroupe($unGrp);
            $laVue->setActionRecue("modifier");
            $laVue->setActionAEnvoyer("validerModifier");
            $laVue->setMessage("Modifier le groupe : " . $unGrp->getNom() . " (" . $unGrp->getId() . ")");
            parent::controlerVueAutorisee();
            $laVue->setTitre("Festival - Groupes");
            $this->vue->afficher();
        }
    }

    /** controleur= Groupes & action=supprimer & id=identifiant_établissement
     * Supprimer un établissement d'après son identifiant     */
    public function supprimer() {
        $idGrp = $_GET["id"];
        $this->vue = new VueSupprimerGroupe();
        // Lire dans la BDD les données de l'établissement à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($idGrp));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }

    /** controleur= Groupes & action= validerSupprimer
     * supprimer un établissement dans la base de données après confirmation   */
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant de l'établissement à supprimer");
        } else {
            // suppression de l'établissement d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des établissements
        header("Location: index.php?controleur=groupes&action=liste");
    }
    /**
     * Vérification des données du formulaire de saisie
     * @param Groupe $unGroupe établissement à vérifier
     * @param bool $creation : =true si formulaire de création d'un nouvel établissement ; =false sinon
     */
    private function verifierDonneesEtab(Groupe $unGroupe, bool $creation) {
        // Vérification des champs obligatoires.
        // Dans le cas d'une création, on vérifie aussi l'id
        if (($creation && $unGroupe->getId() == "") || $unGroupe->getNom() == "" || 
                $unGroupe->getNbPers() == "" || $unGroupe->getNomPays() == "" || $unGroupe->getHebergement() == "") {
            GestionErreurs::ajouter('Chaque champ suivi du caractère * est obligatoire');
        }
        // En cas de création, vérification du format de l'id et de sa non existence
        if ($creation && $unGroupe->getId() != "") {
            // Si l'id est constitué d'autres caractères que de lettres non accentuées 
            // et de chiffres, une erreur est générée
            if (!estAlphaNumerique($unGroupe->getId())) {
                GestionErreurs::ajouter("L'identifiant doit comporter uniquement des lettres non accentuées et des chiffres");
            } else {
                if (GroupeDAO::isAnExistingId($unGroupe->getId())) {
                    GestionErreurs::ajouter("L'établissement " . $unGroupe->getId() . " existe déjà");
                }
            }
        }
        // Vérification qu'un établissement de même nom n'existe pas déjà (id + nom si création)
        if ($unGroupe->getNom() != "" && GroupeDAO::isAnExistingName($creation, $unGroupe->getId(), $unGroupe->getNom())) {
            GestionErreurs::ajouter("L'établissement " . $unGroupe->getNom() . " existe déjà");
        }
        // Vérification du format du code postal
    }

    /*****************************************************************************
     * Méthodes permettant de préparer les informations à destination des vues
     ******************************************************************************/

    /**
     * Retourne la liste de tous les Groupes et du nombre d'attributions de chacun
     * @return Array tableau associatif à 2 dimensions : 
     *      - dimension 1, l'index est l'id de l'établissement
     *      - dimension 2, index "etab" => objet de type Groupe
     *      - dimension 2, index "nbAttrib" => nombre d'attributions pour cet établissement
     */
    public function getTabGroupesAvecNbAttributions(): Array {
        $lesGroupesAvecNbAttrib = Array();
        $lesGroupes = GroupeDAO::getAll();
        foreach ($lesGroupes as $unGroupe) {
            /* @var Groupe $unGroupe */
            $lesGroupesAvecNbAttrib[$unGroupe->getId()]['grp'] = $unGroupe;
            $lesGroupesAvecNbAttrib[$unGroupe->getId()]['nbAttrib'] = count(AttributionDAO::getAllByIdEtab($unGroupe->getId()));
        }
        return $lesGroupesAvecNbAttrib;
    }

}