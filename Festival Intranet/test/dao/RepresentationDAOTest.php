<!doctype html>
<?php

use modele\dao\RepresentationDAO;
use modele\dao\Bdd;
use modele\metier\Groupe;
use modele\metier\Lieu;
use modele\metier\Representation;
use modele\dao\LieuDAO;
use controleur\Session;

require_once __DIR__ . '/../../includes/autoload.inc.php';
Session::demarrer();
Bdd::connecter();
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>RepresentationDAO Test</title>
    </head>

    <body>

        <?php
        // Jeu d'essai
        $id = '3';
        echo "<h2>Test de RepresentationDAO</h2>";

        // Test n°1
        echo "<h3>1- getOneById</h3>";
        $objet = RepresentationDAO::getOneById($id);
        var_dump($objet);

        // Test n°2
        echo "<h3>2- getAll</h3>";
        $lesObjets = RepresentationDAO::getAll();
        var_dump($lesObjets);

        $id = '999';
        // Test n°3
        echo "<h3>3- insert</h3>";
        try {

            $unGroupe = new Groupe('g001', 'les beaux gosses', 'général Talcazar', 'Tapiocopolis', 25, 'san Theodores', 'N');
            $unLieu = new Lieu('1', 'LE PARC DES ANIMAUX', '14 rue des animaux', '3000');
            $objet1 = new Representation($id, $unLieu, $unGroupe, '2017-07-11', '21:30', '23:00');
            $ok = RepresentationDAO::insert($objet1);
            if ($ok) {
                echo "<h4>ooo réussite de l'insertion ooo</h4>";
                $objetLu = RepresentationDAO::getOneById($id);
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de l'insertion ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°4
        echo "<h3>4- update</h3>";
        try {
            $objet1->setLieu(LieuDAO::getOneById(2));
            $objet1->setGroupe(modele\dao\GroupeDAO::getOneById("g003"));
            $objet1->setHeuredebut("20:30");
            $ok = RepresentationDAO::update($id, $objet1);
            if ($ok) {
                $objetLu = RepresentationDAO::getOneById($id);
                echo "<h4>ooo réussite de la mise à jour ooo</h4>";
                var_dump($objetLu);
            } else {
                echo "<h4>*** échec de la mise à jour, le nombre de chambres n'est pas le bon ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête, erreur PDO ***</h4>" . $e->getMessage();
        }

        // Test n°5
        echo "<h3>5- delete</h3>";
        try {
            $ok = RepresentationDAO::delete($id);
            if ($ok) {
                echo "<h4>ooo réussite de la suppression ooo</h4>";
            } else {
                echo "<h4>*** échec de la suppression ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }


        // Test n°6
        echo "<h3>6- isAnExistingId</h3>";
        try {
            $id = "3";
            $ok = RepresentationDAO::isAnExistingId($id);
            $ok = $ok && !RepresentationDAO::isAnExistingId('AZERTY');
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°7
        echo "<h3>7- isAnExistingRep</h3>";
        try {
            $lieu = LieuDAO::getOneById(1);
            $grp = modele\dao\GroupeDAO::getOneById("g001");
            $rep = new Representation(1,$lieu,$grp,"2017-07-11","20:00","21:45");
            //Création
            $ok = RepresentationDAO::isAnExistingRep(true,$rep);
            if (!$ok) {
                echo "<h4>ooo test réussi (Création) ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
            //Modification
            $ok = RepresentationDAO::isAnExistingRep(false,$rep);
            if (!$ok) {
                echo "<h4>ooo test réussi (Modification) ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }

        // Test n°8
        echo "<h3>8- isAnExistingGrp</h3>";
        try {
            $grp = new Groupe("g001","Groupe folklorique du Bachkortostan","","",40,"Bachkirie","O");
            $ok = RepresentationDAO::isAnExistingGrp(true,$grp);
            
            if ($ok) {
                echo "<h4>ooo test réussi ooo</h4>";
            } else {
                echo "<h4>*** échec du test ***</h4>";
            }
        } catch (Exception $e) {
            echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
        }
        ?>
    </body>
</html>


