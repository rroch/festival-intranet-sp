CREATE TABLE Representation
(id char(5) not null,
id_lieu varchar(45) not null,
id_groupe varchar(45) not null,
daterep date not null,
heuredebut varchar(10) not null,
heurefin varchar(10) not null,
constraint pk_Representation primary key(id),
constraint fk1_Representation1 FOREIGN KEY(id_groupe) references Groupe(id),
constraint fk2_Representation2 FOREIGN KEY(id_lieu) references Lieu(id))
ENGINE=INNODB;




