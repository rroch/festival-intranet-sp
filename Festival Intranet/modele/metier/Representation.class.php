<?php

namespace modele\metier;

class Representation {

    /**
     *
     * @var int
     */
    private $id;

    /**
     *
     * @var Lieu
     */
    private $lieu;

    /**
     *
     * @var Groupe
     */
    private $groupe;

    /**
     *
     * @var string
     */
    private $daterep;

    /**
     *
     * @var string
     */
    private $heuredebut;

    /**
     *
     * @var string
     */
    private $heurefin;

    /**
     * Constructeur de l'objet Représentation
     * @param int $id id de Représentation
     * @param Lieu $lieu lieu de Représentation
     * @param Groupe $groupe groupe de la Représentatrion
     * @param string $daterep date de la représentation
     * @param string $heuredebut heure du début de la représentation
     * @param string $heurefin heure de fin de la représentation
     */
    function __construct($id, $lieu, $groupe, $daterep, $heuredebut, $heurefin) {
        $this->id = $id;
        $this->lieu = $lieu;
        $this->groupe = $groupe;
        $this->daterep = $daterep;
        $this->heuredebut = $heuredebut;
        $this->heurefin = $heurefin;
    }

    // Getters and Setters
    function getId(): int {
        return $this->id;
    }

    function getLieu(): Lieu {
        return $this->lieu;
    }

    function getGroupe(): Groupe {
        return $this->groupe;
    }

    function getDaterep(): string {
        return $this->daterep;
    }

    function getHeuredebut(): string {
        return $this->heuredebut;
    }

    function getHeurefin(): string {
        return $this->heurefin;
    }

    function setId(int $id) {
        $this->id = $id;
    }

    function setLieu(Lieu $lieu) {
        $this->lieu = $lieu;
    }

    function setGroupe(Groupe $groupe) {
        $this->groupe = $groupe;
    }

    function setDaterep(string $daterep) {
        $this->daterep = $daterep;
    }

    function setHeuredebut(string $heuredebut) {
        $this->heuredebut = $heuredebut;
    }

    function setHeurefin(string $heurefin) {
        $this->heurefin = $heurefin;
    }


}
