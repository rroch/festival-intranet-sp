<?php

namespace modele\dao;

use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use PDO;

/**
 * Description of RepresentationDAO
 * Classe métier :  Representation
 * @author rroch
 * @version 2020
 */
class RepresentationDAO {

    /**
     * crée un objet métier à partir d'un enregistrement de la table REPRESENTATION et des tables liées
     * @param array $enreg 
     * @return Représentation objet métier obtenu
     */
    protected static function enregVersMetier($enreg) {
        $id = $enreg['ID'];
        $lieu = LieuDAO::getOneById($enreg['ID_LIEU']);
        $groupe = GroupeDAO::getOneById($enreg['ID_GROUPE']);
        $dateRep = $enreg['DATEREP'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        // instancier l'objet Offre
        $objetMetier = new Representation($id, $lieu, $groupe, $dateRep, $heureDebut, $heureFin);

        return $objetMetier;
    }

    /**
     * Complète une requête préparée
     * les paramètres de la requête associés aux valeurs des attributs d'un objet métier
     * @param Representation $objetMetier
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Representation $objetMetier, \PDOStatement $stmt) {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        /* @var $etab Etablissement */
        //$lieu = $objetMetier->getLieu();
        /* @var $typeCh TypeChambre */
        //$groupe = $objetMetier->getGroupe();
        $stmt->bindValue(':id', $objetMetier->getId());
        $stmt->bindValue(':id_lieu', $objetMetier->getLieu()->getId());
        $stmt->bindValue(':id_groupe', $objetMetier->getGroupe()->getId());
        $stmt->bindValue(':daterep', $objetMetier->getDateRep());
        $stmt->bindValue(':heuredebut', $objetMetier->getHeureDebut());
        $stmt->bindValue(':heurefin', $objetMetier->getHeureFin());
    }

    /**
     * Retourne la liste de toutes les représentations
     * @return array tableau d'objets de type Représentations
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY daterep DESC, heuredebut DESC";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Construire un objet d'après son identifiant, à partir des des enregistrements de la table Représentation
     * L'identifiant de la table Représentation est composé : ($idEtab, $idTypeChambre)
     * @param string $idEtab identifiant de l'établissement émetteur de la Représentation
     * @param string $idTypeChambre identifiant du type de chambre concerné par la Représentation
     * @return Representation : objet métier si trouvé dans la BDD, null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string $idEtab identifiant de l'établissement émetteur de l'offre
     * @param string $idTypeCh identifiant du type de chambre concerné par l'offre
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation "
                . " WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $ok = false;
        $stmt = Bdd::getPdo()->prepare("INSERT INTO Representation VALUES(:id, :id_lieu, :id_groupe, :daterep, :heuredebut, :heurefin)");
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mise à jour d'une représentation
     * @param string $idEtab identifiant de l'établissement concerné par l'offre
     * @param string $idTypeCh identifiant du type de chambre concerné par l'offre
     * @param int $nb nouveau nombre de chambre
     * @return boolean =true si la mise à jour a été correcte
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $stmt = Bdd::getPdo()->prepare("UPDATE  Representation SET ID_LIEU=:id_lieu, ID_GROUPE=:id_groupe, DATEREP=:daterep, HEUREDEBUT=:heuredebut, HEUREFIN=:heurefin WHERE ID = :id ");
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Permet de vérifier s'il existe ou non une representation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la representation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $stmt = Bdd::getPdo()->prepare("SELECT COUNT(*) FROM Representation WHERE id =:id");
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

    /**
     * Permet de vérifier si le lieu de la représentation est déjà sur une autre représentation à la même heure
     * @param bool $creation savoir si c'est une nouvelle représentation ou un modification
     * @param Representation $uneRep l'objet représentation
     * @return boolean =true si la lieu est déjà utilisé, =false sinon
     */
    public static function isAnExistingRep($creation, $uneRep) {
        // S'il s'agit d'une création, on vérifie juste la non existence du nom sinon
        // on vérifie la non existence d'un autre établissement (id!='$id') portant
        // le même nom
        $ok = false;
        $id = $uneRep->getId();
        $hrFn = $uneRep->getHeureFin();
        $hrDbt = $uneRep->getHeureDebut();
        $date = $uneRep->getDateRep();
        $id_lieu = $uneRep->getLieu()->getId();

        if ($creation) {
            $stmt = Bdd::getPdo()->prepare("SELECT COUNT(*) FROM Representation WHERE id_lieu = :id_lieu AND dateRep = :dateRep AND (heureDebut >= :hrDbt AND heureDebut < :hrFn AND heureFin > :hrDbt AND heureFin <= :hrFn OR :hrDbt >= heureDebut AND :hrDbt < heureFin OR :hrFn > heureDebut AND :hrFn <= heureFin)");
            $stmt->bindParam(':id_lieu', $idLieu);
            $stmt->bindParam(':hrDbt', $hrDbt);
            $stmt->bindParam(':hrFn', $hrFn);
            $stmt->bindParam(':dateRep', $date);
            $stmt->execute();
        } else {
            $stmt = Bdd::getPdo()->prepare("SELECT COUNT(*) FROM Representation WHERE id <> :id AND id_lieu = :id_lieu AND daterep = :dateRep AND (heureDebut >= :hrDbt AND heureDebut < :hrFn AND heureFin > :hrDbt AND heureFin <= :hrFn OR :hrDbt >= heureDebut AND :hrDbt < heureFin OR :hrFn > heureDebut AND :hrFn <= heureFin)");
            $stmt->bindParam(':id', $id);
            $stmt->bindParam(':id_lieu', $idLieu);
            $stmt->bindParam(':hrDbt', $hrDbt);
            $stmt->bindParam(':hrFn', $hrFn);
            $stmt->bindParam(':dateRep', $date);
            $stmt->execute();
        }
        if ($stmt->fetchColumn(0)) {
            $ok = true;
        }
        return $ok;
    }

    /**
     * Permet de vérifier si le groupe n'a pas déjà une représentation
     * @param bool $creation savoir si c'est une nouvelle représentation ou un modification
     * @param Groupe $groupe groupe de la représentation
     * @return boolean =true si la groupe est déjà présent dans la BDD, =false sinon
     */
    public static function isAnExistingGrp($creation, $groupe) {
        $ok = false;
        $idGrp = $groupe->getId();
        if ($creation) {
            $stmt = Bdd::getPdo()->prepare("SELECT COUNT(*) FROM Representation WHERE id_groupe =:idgroupe");
            $stmt->bindParam(':idgroupe', $idGrp);
            $stmt->execute();
            if ($stmt->fetchColumn(0)) {
                $ok = true;
            }
        }
        return $ok;
    }

}
