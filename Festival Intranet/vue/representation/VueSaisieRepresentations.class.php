<?php

namespace vue\representation;

use vue\VueGenerique;
use modele\metier\Representation;

/**
 * Description Page de saisie/modification d'une représentation
 * @author rroch
 * @version 2020
 */
class VueSaisieRepresentations extends VueGenerique {

    /** @var Representation à afficher */
    private $uneRepresentation;

    /** @var Array de Lieu */
    private $lesLieux;

    /** @var Array de Groupe */
    private $lesGroupes;

    /** @var string ="creer" ou = "modifier" en fonction de l'utilisation du formulaire */
    private $actionRecue;

    /** @var string ="validerCreer" ou = "validerModifier" en fonction de l'utilisation du formulaire */
    private $actionAEnvoyer;
    
    /** @var string à afficher en tête du tableau */
    private $message;

    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <form method="POST" action="index.php?controleur=representations&action=<?= $this->actionAEnvoyer ?>">
            <br>
            <table width="85%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->message ?></strong></td>
                </tr>

                <?php
                // En cas de création, l'id est accessible à la saisie
                if ($this->actionRecue == "creer") {
                    // On a le souci de ré-afficher l'id tel qu'il a été saisi
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td> Id*: </td>
                        <td><input type="text" value="<?= $this->uneRepresentation->getId() ?>" name="id" size ="10" maxlength="8"></td>
                    </tr>
                    <?php
                } else {
                    // sinon l'id est dans un champ caché
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->uneRepresentation->getId(); ?>" name="id"></td><td></td>
                    </tr>
                    <?php
                }
                ?> 
                <tr class="ligneTabNonQuad">
                    <td> Lieu*: </td>
                    <td>
                        <select name="id_lieu">
                            <option selected value="" > Choisir un lieu</option>
                            <?php
                            foreach ($this->lesLieux as $lieux) {
                                if($this->uneRepresentation->getLieu()->getId() ==  $lieux->getId()){
                                    echo '<option selected value ="' . $lieux->getId() . '">' . $lieux->getNom() . '</option>';
                                } else{
                                    echo '<option '.' value ="' . $lieux->getId() . '">' . $lieux->getNom() . '</option>';
                                }
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                
                <tr class="ligneTabNonQuad">
                    <td> Groupe*: </td>
                    <td>
                        <select name="id_groupe">
                            <option selected value=""> Choisir un groupe</option>
                            <?php
                            foreach ($this->lesGroupes as $groupes) {
                                if($this->uneRepresentation->getGroupe()->getId() ==  $groupes->getId()){
                                    echo '<option selected value ="' . $groupes->getId() . '">' . $groupes->getNom() . '</option>';
                                } else{
                                    echo '<option value ="' . $groupes->getId() . '">' . $groupes->getNom() . '</option>';
                                }
                                
                            }
                            ?>
                        </select>
                    </td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> date*: </td>
                    <td><input type="date" value="<?= $this->uneRepresentation->getDateRep() ?>" name="daterep" 
                               size="12" maxlength="10"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure Début*: </td>
                    <td><input type="time" value="<?= $this->uneRepresentation->getHeureDebut() ?>" name="heuredebut" 
                               size="7" maxlength="8"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td> Heure Fin*: </td>
                    <td><input type="time" value="<?= $this->uneRepresentation->getHeureFin() ?>" name="heurefin" size="40" 
                               maxlength="8"></td>
                </tr>
            </table>

            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler">
                    </td>
                </tr>
            </table>
            <a href="index.php?controleur=representations&action=liste">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }

    function setUneRepresentation(Representation $uneRepresentation) {
        $this->uneRepresentation = $uneRepresentation;
    }

    public function setActionRecue(string $action) {
        $this->actionRecue = $action;
    }

    public function setActionAEnvoyer(string $action) {
        $this->actionAEnvoyer = $action;
    }

    public function setMessage(string $message) {
        $this->message = $message;
    }

    function setLesLieux(Array $lesLieux) {
        $this->lesLieux = $lesLieux;
    }

    function setLesGroupes(Array $lesGroupes) {
        $this->lesGroupes = $lesGroupes;
    }

}

